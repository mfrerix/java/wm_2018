import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WMCalculator {

    private List<Group> groups = new ArrayList<>();

    public static void main(String[] args) {
        System.out.println("======================== Gruppenphase ========================");

        Group groupA = new Group("A");
        System.out.println("Gewinner Gruppe A = " + groupA.getTeams().get(0));
        Group groupB = new Group("B");
        System.out.println("Gewinner Gruppe B = " + groupB.getTeams().get(0));
        Group groupC = new Group("C");
        System.out.println("Gewinner Gruppe C = " + groupC.getTeams().get(0));
        Group groupD = new Group("D");
        System.out.println("Gewinner Gruppe D = " + groupD.getTeams().get(0));
        Group groupE = new Group("E");
        System.out.println("Gewinner Gruppe E = " + groupE.getTeams().get(0));
        Group groupF = new Group("F");
        System.out.println("Gewinner Gruppe F = " + groupF.getTeams().get(0));
        Group groupG = new Group("G");
        System.out.println("Gewinner Gruppe G = " + groupG.getTeams().get(0));
        Group groupH = new Group("H");
        System.out.println("Gewinner Gruppe H = " + groupH.getTeams().get(0));

        System.out.println("======================== K.O. Phase ========================");
        System.out.println("======================== Achtelfinale ========================");
        System.out.println("Achtelfinal 1");
        Match achtelfinal1 = new Match(groupA.getTeams().get(0), groupB.getTeams().get(1), false);
        System.out.println(achtelfinal1.toString());

        System.out.println("\nAchtelfinal 2");
        Match achtelfinal2 = new Match(groupC.getTeams().get(0), groupD.getTeams().get(1), false);
        System.out.println(achtelfinal2.toString());

        System.out.println("\nAchtelfinal 3");
        Match achtelfinal3 = new Match(groupB.getTeams().get(0), groupA.getTeams().get(1), false);
        System.out.println(achtelfinal3.toString());

        System.out.println("\nAchtelfinal 4");
        Match achtelfinal4 = new Match(groupD.getTeams().get(0), groupC.getTeams().get(1), false);
        System.out.println(achtelfinal4.toString());

        System.out.println("\nAchtelfinal 5");
        Match achtelfinal5 = new Match(groupE.getTeams().get(0), groupF.getTeams().get(1), false);
        System.out.println(achtelfinal5.toString());

        System.out.println("\nAchtelfinal 6");
        Match achtelfinal6 = new Match(groupG.getTeams().get(0), groupH.getTeams().get(1), false);
        System.out.println(achtelfinal6.toString());


        System.out.println("\nAchtelfinal 7");
        Match achtelfinal7 = new Match(groupF.getTeams().get(0), groupE.getTeams().get(1), false);
        System.out.println(achtelfinal7.toString());

        System.out.println("\nAchtelfinal 8");
        Match achtelfinal8 = new Match(groupH.getTeams().get(0), groupG.getTeams().get(1), false);
        System.out.println(achtelfinal8.toString());

        System.out.println("======================== Viertelfinal ========================");
        System.out.println("Viertelfinal 1");
        Match viertelfinale1 = new Match(achtelfinal1.getWinner(),achtelfinal2.getWinner(),false);
        System.out.println(viertelfinale1.toString());

        System.out.println("\nViertelfinal 2");
        Match viertelfinale2 = new Match(achtelfinal5.getWinner(),achtelfinal6.getWinner(),false);
        System.out.println(viertelfinale2.toString());

        System.out.println("\nViertelfinal 3");
        Match viertelfinale3 = new Match(achtelfinal3.getWinner(),achtelfinal4.getWinner(),false);
        System.out.println(viertelfinale3.toString());

        System.out.println("\nViertelfinal 4");
        Match viertelfinale4 = new Match(achtelfinal7.getWinner(),achtelfinal8.getWinner(),false);
        System.out.println(viertelfinale4.toString());

        System.out.println("======================== Halbfinale ========================");
        System.out.println("Halbfinale 1");
        Match halbfinale1 = new Match(viertelfinale1.getWinner(),viertelfinale2.getWinner(),false);
        System.out.println(halbfinale1.toString());

        System.out.println("\nHalbfinale 2");
        Match halbfinale2 = new Match(viertelfinale3.getWinner(),viertelfinale4.getWinner(),false);
        System.out.println(halbfinale2.toString());

        System.out.println("======================== Finale ========================");
        Match finale = new Match(halbfinale1.getWinner(),halbfinale2.getWinner(),false);
        System.out.println(finale.toString());

        List<Team> allTeams = new ArrayList<>();
        allTeams.addAll(groupA.getTeams());
        allTeams.addAll(groupB.getTeams());
        allTeams.addAll(groupC.getTeams());
        allTeams.addAll(groupD.getTeams());
        allTeams.addAll(groupE.getTeams());
        allTeams.addAll(groupF.getTeams());
        allTeams.addAll(groupG.getTeams());
        allTeams.addAll(groupH.getTeams());

        Collections.sort(allTeams, Collections.reverseOrder((team1,team2) -> Integer.compare(team1.getGoals(), team2.getGoals())));
        System.out.println("\nMeiste Tore: " + allTeams.get(0) + " mit " + allTeams.get(0).getGoals() + " Tore");


    }




}
