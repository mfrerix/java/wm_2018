public class Match {

    private static int POINTS_WIN = 3;

    private static int POINTS_DRAW = 1;

    private static int POINTS_LOSE = 0;

    private Team home;

    private Team guest;
    private boolean allowDraw;

    private int goalsHome;

    private int goalsGuest;

    private Team winner;

    public Match(Team home, Team guest, boolean allowDraw) {
        this.home = home;
        this.guest = guest;
        this.allowDraw = allowDraw;

        goalsHome = 0;
        goalsGuest = 0;
        calculateMatch();
    }

    public Team getWinner() {
        if(winner == null) {
            calculateMatch();
        }
        return winner;
    }

    private void calculateMatch() {

        do {
            goalsHome = GoalCalculator.generateGoalNumber();
            goalsGuest = GoalCalculator.generateGoalNumber();
        }while(!allowDraw && goalsHome == goalsGuest);

        if(goalsHome > goalsGuest) {
            home.addPoints(POINTS_WIN);
            guest.addPoints(POINTS_LOSE);
            winner = home;
        }
        else if (goalsHome == goalsGuest) {
            home.addPoints(POINTS_DRAW);
            guest.addPoints(POINTS_DRAW);
        }else {
            guest.addPoints(POINTS_WIN);
            home.addPoints(POINTS_LOSE);
            winner = guest;
        }

        home.addGoals(goalsHome);
        home.addEnemyGoals(goalsGuest);
        guest.addGoals(goalsGuest);
        guest.addEnemyGoals(goalsHome);
    }

    @Override
    public String toString() {
        return home.getName() + " " + goalsHome + " : " + goalsGuest + " " + guest.getName();
    }
}
