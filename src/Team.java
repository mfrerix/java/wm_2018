public class Team {

    private String name;
    private int points;

    private int goals;
    private int enemyGoals;


    public Team(String name) {
        this.goals = 0;
        this.enemyGoals = 0;
        this.points = 0;
        this.name = name;
    }

    public int getGoals() {
        return goals;
    }

    public String getName() {
        return name;
    }

    public void addGoals(int numberOfGoals) {
        this.goals += numberOfGoals;
    }

    public void addEnemyGoals(int numberOfGoals) {
        this.enemyGoals += numberOfGoals;
    }

    public int getEnemyGoals() {
        return enemyGoals;
    }

    public int getPoints() {
        return points;
    }

    public void addPoints(int points) {
        this.points += points;
    }

    @Override
    public String toString() {
        return getName();
    }
}
