import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TeamLoader {

    public static List<Team> loadTeams(String name) {
        List<Team> teams = new ArrayList<>();
        FileReader fileReader;
        try {
            fileReader = new FileReader("groups/group_" + name.toLowerCase());

            BufferedReader br = new BufferedReader(fileReader);
            String teamName;
            while((teamName = br.readLine()) != null) {
                teams.add(new Team(teamName));
            }
        } catch (Exception ex) {
            Logger.getLogger(TeamLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return teams;
    }
}
