import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Group {

    private String name;

    private List<Team> teams;

    public Group(String name) {
        System.out.println("\nGroup " + name);
        this.name = "Gruppe " + name;
        teams = TeamLoader.loadTeams(name);
        calcGroupResult();
        Collections.sort(teams, new TeamComparator());

    }

    private void calcGroupResult() {
        for(int i = 0; i < teams.size() - 1; i++) {
            for(int j = i+1; j < teams.size(); j++) {
                Match match = new Match(teams.get(i), teams.get(j), true);
                System.out.println(match.toString());
            }
        }
    }

    public String getName() {
        return name;
    }

    public List<Team> getTeams() {

        Collections.sort(teams, Collections.reverseOrder(new TeamComparator()));
        return teams;
    }






}
