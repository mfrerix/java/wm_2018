public class GoalCalculator {

    public static int generateGoalNumber() {
        int number = (int)((Math.random()*100) + 1);
        if(number >= 0 && number < 30) {
            return 0;
        }else if(number >= 30 && number <60) {
            return 1;
        } else if(number >= 60 && number < 85) {
            return 2;
        } else if(number >= 85 && number < 95) {
            return 3;
        } else if (number >= 95 && number < 99) {
            return 4;
        }
        else {
            return 5;
        }
    }

}
