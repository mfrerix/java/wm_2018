import java.util.Comparator;

public class TeamComparator implements Comparator<Team> {
    @Override
    public int compare(Team team1, Team team2) {
        if(team1.getPoints() == team2.getPoints()) {
            int goalDifferentTeam1 = team1.getGoals() - team1.getEnemyGoals();
            int goalDifferentTeam2 = team2.getGoals() - team2.getEnemyGoals();
            if(goalDifferentTeam1 == goalDifferentTeam2) {
                return Integer.compare(team1.getGoals(),team2.getGoals());
            } else {
                return Integer.compare(goalDifferentTeam1,goalDifferentTeam2);
            }
        } else {
            return Integer.compare(team1.getPoints(), team2.getPoints());
        }
    }
}
